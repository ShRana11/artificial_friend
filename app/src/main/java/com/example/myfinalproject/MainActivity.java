package com.example.myfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private PaintView paintView;
    private TextView clock;
    private TextView things;
    private TextView results;
   // Intent i;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        paintView = (PaintView) findViewById(R.id.draw1);
        clock = (TextView) findViewById(R.id.clock1);
        things = (TextView)findViewById(R.id.thing1);
        results = (TextView)findViewById(R.id.result1);
        DisplayMetrics matrix = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(matrix);
        Intent myIntent = new Intent(this, AllDrawingActivity.class);
        paintView.init(matrix, clock, things,results, myIntent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.normal:
                paintView.normal();
                return true;
            case R.id.clear:
                paintView.clear();
                return true;
            case R.id.emboss:
                paintView.emboss();
                return true;
            case R.id.blur:
                paintView.blur();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
