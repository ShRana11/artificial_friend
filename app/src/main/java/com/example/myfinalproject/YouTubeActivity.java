package com.example.myfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Random;

public class YouTubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private static final int RECOVERY_REQUEST = 1;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    public static final String TAG = "JENELLE";
    ArrayList<String> youtubelink = new ArrayList<>();
    String show;
    private YouTubePlayerView youTubeView;
    public int counter = 0;
    Switch s1;
    Button playAgain;
    boolean playVid = false;
    Button Chatting;
    Handler mhandler = new Handler();
    Random random = new Random();

    private YouTubePlayer player;
    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;
//    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube);
        youtubelink.clear();
        Chatting = (Button)findViewById(R.id.chat);
        playAgain = (Button)findViewById(R.id.playing);
        db.collection("youLink")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                          //  Log.d(TAG,"finished querying firebase");
                            for (QueryDocumentSnapshot document : task.getResult()) {
                           //     Log.d(TAG, document.getId() + " => " + document.getData());
                                int m = 1;
                               int x = document.getData().size();
                              //  Log.d(TAG, "size: " + x);
                               for(int i = 0; i < x;i++){
                                  String index = String.valueOf(m);
                                  youtubelink.add(document.getData().get(index).toString());
                                   m = m+1;
                               }
                                for(int i = 0; i< youtubelink.size();i++){
                                   String xx = youtubelink.get(i);
                                 //   Log.d(TAG, "++Current player name: " + xx);
                                    playVid = true;
                                }


                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(Config.YOUTUBE_API_KEY, this);
        mToast.run();

    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.player = player;

    }
    public void playvideo(){

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.error), youTubeInitializationResult.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

//
    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying() {
            // Called when playback starts, either due to user action or call to play().

        }

        @Override
        public void onPaused() {
            // Called when playback is paused, either due to user action or call to pause().

        }

        @Override
        public void onStopped() {

        }

        @Override
        public void onBuffering(boolean b) {
            // Called when buffering starts or ends.
        }

        @Override
        public void onSeekTo(int i) {
            // Called when a jump in playback position occurs, either
            // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
        }
    }
    public void chat(View view){
        Intent s = new Intent(YouTubeActivity.this, ChatActivity.class);
        s.putExtra("data", "Did you enjoyed Video(Yes/No)?");
        s.putExtra("check", "video");
        startActivity(s);


    }
    public void setReplay(View view){
        Intent s = new Intent(this, YouTubeActivity.class);
        startActivity(s);
    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
            // Called when the player is loading a video
            // At this point, it's not ready to accept commands affecting playback such as play() or pause()
        }

        @Override
        public void onLoaded(String s) {
            // Called when a video is done loading.
            // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
            if(!TextUtils.isEmpty(s) && player != null)
            player.seekToMillis(77000);
            player.play(); //auto play

        }

        @Override
        public void onAdStarted() {
            // Called when playback of an advertisement starts.
        }

        @Override
        public void onVideoStarted() {
            // Called when playback of the video starts.
        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            // Called when an error occurs.
        }
    }
//
    public final Runnable mToast = new Runnable() {
        @Override
        public void run() {
            mhandler.postDelayed(this, 1000);
            if(playVid == true){
            int number = random.nextInt(youtubelink.size());
            String link = youtubelink.get(number);
            player.cueVideo(link);
            playVid = false;
            }


        }
    };
}

