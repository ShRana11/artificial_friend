package com.example.myfinalproject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class imageAdapter extends BaseAdapter {
    private Context context;
    int pos[];
    int res[];
    int time = 0;

    public imageAdapter(Context context){
    this.context = context;

    }
    public void time(int t){
        this.time = t;
    }
    @Override
    public int getCount() {
        return 9;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public int[] getposArray(int[] pos){
        this.pos = pos;
        return pos;
    }
    public int[] getResArray(int[] res){
        this.res = res;
        return res;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = null;

            if (convertView == null) {
                imageView = new ImageView(this.context);
                imageView.setLayoutParams(new GridLayout.LayoutParams());
                imageView.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
                imageView.setPivotX(100);
                imageView.setPadding(100, 100, 8, 8);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else {
                imageView = (ImageView) convertView;

            }
        if (time == 0) {
            imageView.setImageResource(res[pos[position]]);
            return imageView;
        } else {
            imageView.setImageResource(R.drawable.a0);
            return imageView;
        }

    }
}
