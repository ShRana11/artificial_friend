package com.example.myfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        final RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.rlayout);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.launcher);
        imageView.setAnimation(animation);
        animation.setDuration(5000);


        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                finish();
                Intent x = new Intent(LauncherActivity.this,ChatActivity.class);
                x.putExtra("data", "BOT : Hi");
                x.putExtra("check", "");
                startActivity(x);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                recreate();

            }
        });
    }
}
