package com.example.myfinalproject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;


public class PaintView extends View implements Runnable {



    public static int brush_size = 10;
    private static final int PIXEL_WIDTH = 28;
    Timer timer;
    int firstTime  = 0;
    DisplayMetrics m = new DisplayMetrics();

    Things thing = new Things();
    String type = "";
    byte[] allImages = new byte[1];
    byte[] allImages1 = new byte[1];
    byte[] allImages2 = new byte[1];
   // public List<Bitmap>
    boolean mtimer = false;
    Handler mhandkler = new Handler();
    Random num = new Random();
    public static final int default_color = Color.BLACK;
    public static final int default_backcolor = Color.WHITE;
    public static final float touch_tolerance = 4;
    private float aX, aY;
    private List<Classifier> mClassifiers = new ArrayList<>();
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;
    private Path mpath;
    private Paint mpaint;
    private ArrayList<Drawing> paths = new ArrayList<>();
    private ArrayList<Drawing> times = new ArrayList<>();
    private int currentColor;
    private int currentbackgroundColor = default_backcolor;
    private int strokeWidth;
    private boolean emboss;
    private boolean blur;
    private MaskFilter mEmboss;
    private  MaskFilter mBlur;
    private Bitmap mBitmap;
    Intent myIntent;
    int x = 10;
    int imageNo = 0;
    TextView clock;
    TextView result;
    TextView things;
    private Canvas mCanvas;
    Canvas checkCan;
    String PreviousImageName;
    Context contextIntent;
    private Paint mBitmapPaint = new Paint(Paint.DITHER_FLAG);
    public PaintView(Context context) {
        super(context, null);
    }



    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        contextIntent = context;
        mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setDither(true);
        mpaint.setColor(default_color);
        mpaint.setStyle(Paint.Style.STROKE);
        mpaint.setStrokeCap(Paint.Cap.ROUND);
        mpaint.setStrokeJoin(Paint.Join.ROUND);
        mpaint.setXfermode(null);
        mpaint.setAlpha(0xff);



        mEmboss = new EmbossMaskFilter(new float[] {1,1,1}, 0.4f, 6, 3.5f);
        mBlur = new BlurMaskFilter(5, BlurMaskFilter.Blur.NORMAL);
        loadModel(context);
    }

    public final Runnable mToast = new Runnable() {
        @Override
        public void run() {
            if (mtimer == false) {
                mhandkler.postDelayed(this, 1000);
                mBitmapPaint.setTextSize(80);
                mBitmapPaint.setColor(Color.BLUE);

                clock.setText("Time : "+String.valueOf(x));
//                mCanvas.drawText(String.valueOf(x), 100, 100, mBitmapPaint);
                String drawThing[] = thing.getThing();

                x = x - 1;
              //  Log.d("jenelee", "y is:" + x);
                if (x <= 0) {
                    int n = num.nextInt(10);
                    type = drawThing[n];
                    imageStore();
                    float pixels[] = getPixelData();
                    things.setText("Draw\n "+type);
                    if (firstTime == 2) {
                        things.setText("");
                        result.setText("");
                        clock.setText("");
                        mtimer = true;

                        myIntent.putExtra("image1", allImages);
                        myIntent.putExtra("image2", allImages1);
                        myIntent.putExtra("image3", allImages2);
                       // myIntent.putExtra("lives", "l");
                        myIntent.setClass(contextIntent, AllDrawingActivity.class);
                        contextIntent.startActivity(myIntent);

                    }
                    String text = "";
                    String text1 = "";
                    for (Classifier classifier : mClassifiers) {
                        final Classification res = classifier.recognize(pixels);
                        if (res.getLabel().equals(null)) {
                           text = "?";
                        }else{
                            text = String.format("%s","It Is "+ res.getLabel());
                        }
                        // classifier.name()
                        //res.getLabel()
                            text1 = String.format("%s",res.getLabel());
                            //        res.getConf());
                    }
                    if(text1.equals(null)){
                        text1 =  "?";
                    }

                    if( PreviousImageName.equals(text1)){
                        text += "\nYou Win";
                    }else{
                        text += "\nYou Loss";
                    }
                    result.setText(text);
                    PreviousImageName = type;

                    clear();
                    x = 10;
                    imageNo = imageNo + 1;
                }
            }
        }
    };


    private void loadModel(final Context d) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mClassifiers.add(
                            TensorFlowClassifier.create(d.getAssets(), "TensorFlow",
                                    "opt_mnist_convnet-tf.pb", "labels.txt", PIXEL_WIDTH,
                                    "input", "output", true));
//                    mClassifiers.add(
//                            TensorFlowClassifier.create(d.getAssets(), "Keras",
//                                    "opt_mnist_convnet-keras.pb", "labels.txt", PIXEL_WIDTH,
//                                    "conv2d_1_input", "dense_2/Softmax", false));
                } catch (final Exception e) {
                    //if they aren't found, throw an error!
                    throw new RuntimeException("Error initializing classifiers!", e);
                }
            }
        }).start();
    }
    public float[] getPixelData() {
        if (mBitmap == null) {
            return null;
        }
        Bitmap resize = getResizedBitmap(mBitmap, 28, 28);

        int width = resize.getWidth();
        int height = resize.getHeight();

        // Get 28x28 pixel data from bitmap
        int[] pixels = new int[(width) * (height)];
        resize.getPixels(pixels, 0, width, 0, 0, width, height);

        float[] retPixels = new float[pixels.length];
        for (int i = 0; i < pixels.length; ++i) {
            // Set 0 for white and 255 for black pixel
            int pix = pixels[i];
            int b = pix & 0xff;
            retPixels[i] = (float)((0xff - b)/255.0);
        }
        return retPixels;
    }
    public void init(DisplayMetrics matrix, TextView c, TextView t, TextView r, Intent i){
        m = matrix;
        this.myIntent = i;
        this.clock = c;
        this.things = t;
        this.result = r;
        int height = matrix.heightPixels;
        int width = matrix.widthPixels;
        int n = num.nextInt(10);
        String drawThing[] = thing.getThing();
        type = drawThing[n];
        things.setText("Draw\n "+type);
        this.PreviousImageName = type;


        mBitmap = Bitmap.createBitmap(width, height,Bitmap.Config.ARGB_8888);

        mCanvas = new Canvas(mBitmap);
        currentColor = default_color;
        strokeWidth = brush_size;
        mToast.run();

    }
    public void normal(){
        emboss = false;
        blur = false;
    }

    public void emboss(){
        emboss = true;
        blur = false;
    }
    public void blur(){
        emboss = false;
        blur = true;
    }

    public void clear(){
        currentbackgroundColor = default_backcolor;
        paths.clear();
        normal();
        invalidate();
    }
    public void imageStore(){
        Bitmap myBit = mBitmap;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        myBit.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        if(imageNo == 0) {
            this.allImages = byteArray;
        }else if(imageNo == 1) {
            this.allImages1 = byteArray;
        }else if (imageNo == 2){
            this.allImages2 = byteArray;
        } else{
            imageNo = 0;
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {

        // super.onDraw(canvas);
        canvas.save();
        if(mBitmap == null){
         return;
        }
        if(firstTime < 3) {
            if (mBitmap.isRecycled()) {
                firstTime = firstTime + 1;
                int height = m.heightPixels;
                int width = m.widthPixels;


                mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

                mCanvas = new Canvas(mBitmap);
                currentColor = default_color;
                strokeWidth = brush_size;
                // x = 10;
            }

            checkCan = canvas;
            mCanvas.drawColor(currentbackgroundColor);
//            mCanvas.drawText(String.valueOf(x), 100, 100, mBitmapPaint);

            for (Drawing dr : paths) {
                mpaint.setColor(dr.color);
                mpaint.setStrokeWidth(dr.strokeWidth);
                mpaint.setMaskFilter(null);
                if (dr.emboss) {
                    mpaint.setMaskFilter(mEmboss);

                } else if (dr.blur) {
                    mpaint.setMaskFilter(mBlur);

                }
                mCanvas.drawPath(dr.path, mpaint);
            }


            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
            canvas.restore();
        }


    }

    private void touchStart(float x , float y){
        mpath = new Path();
        Drawing dr = new Drawing(currentColor,emboss, blur, strokeWidth, mpath);
        paths.add(dr);

        mpath.reset();
        mpath.moveTo(x,y);
        aX = x;
        aY = y;

    }
    private void touchMove(float x, float y){
        float dx = Math.abs(x - aX);
        float dy = Math.abs(y - aY);

        if((dx >= touch_tolerance) || (dy >= touch_tolerance)){
            mpath.quadTo(aX, aY, (x + aX)/2, (y + aY)/2);
            aY = y;
            aX = x;

        }
    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix1 = new Matrix();
        matrix1.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix1, false);
        bm.recycle();
        return resizedBitmap;
    }
    private  void  touchUp(){
        mpath.lineTo(aX, aY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_UP :
                touchUp();
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                invalidate();
                break;
            case  MotionEvent.ACTION_DOWN :
                touchStart(x, y);
                invalidate();
                break;

        }
        return true;
        //  return super.onTouchEvent(event);
    }

    @Override
    public void run() {

    }
}

