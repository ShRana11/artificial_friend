package com.example.myfinalproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class AllDrawingActivity extends AppCompatActivity {
    byte[] images = new byte[1];
    byte[] images2 = new byte[1];
    byte[] images3 = new byte[1];
    boolean mtimer = false;
    int count = 0;
    Handler mhandkler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_drawing);
        images = getIntent().getByteArrayExtra("image1");
        images2 = getIntent().getByteArrayExtra("image2");
        images3 = getIntent().getByteArrayExtra("image3");
        Bitmap bmp = BitmapFactory.decodeByteArray(images, 0, images.length);
        ImageView result = (ImageView) findViewById(R.id.imageView1);
        result.setImageBitmap(bmp);
        Bitmap bmp1 = BitmapFactory.decodeByteArray(images2, 0, images2.length);
        ImageView result1 = (ImageView) findViewById(R.id.imageView2);
        result1.setImageBitmap(bmp1);
        Bitmap bmp2 = BitmapFactory.decodeByteArray(images3, 0, images3.length);
        ImageView result2 = (ImageView) findViewById(R.id.imageView3);
        result2.setImageBitmap(bmp2);
        mToast.run();
    }

    public final Runnable mToast = new Runnable() {
        @Override
        public void run() {
            if (mtimer == false) {
                mhandkler.postDelayed(this, 1000);
                count = count + 1;
                if(count == 6){
                    mtimer = true;
                    Intent s = new Intent(AllDrawingActivity.this, ChatActivity.class);
                    s.putExtra("data", "Bot: Did You Enjoyed while Draw Things(Yes/No)");
                    s.putExtra("check", "draw");
                    startActivity(s);


                }
            }

        }

    };
}
