package com.example.myfinalproject;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.gigamole.library.PulseView;

import ai.api.AIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Result;

public class ChatActivity extends AppCompatActivity implements AIListener {

    private  static  final int recordeCode = 200;
    ImageView emoji;
    AIService aiService;
    TextView textView;
    TextView textView1;
    Intent intent = new Intent();
    String check = "";
    String data = "";
    PulseView pulseView;
    Handler mhandler = new Handler();
    int timer = 0;
    boolean tim = false;
    String res;
    String query;
    AIConfiguration aiConfiguration;
    AnimationDrawable drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        textView = (TextView)findViewById(R.id.textView);
        textView1 = (TextView)findViewById(R.id.textView1);
        pulseView = (PulseView)findViewById(R.id.pv);
        emoji = (ImageView)findViewById(R.id.emoji);
        emoji.setBackgroundResource(R.drawable.smiley);
        drawable = (AnimationDrawable) emoji.getBackground();


        this.intent = getIntent();
        check = intent.getStringExtra("check");
        data = intent.getStringExtra("data");
        if(check.equals("music")){
            textView.setText(data);
        } else if (check.equals("draw")){
            textView.setText(data);
        }else if (check.equals("video")){
            textView.setText(data);
        }else if (check.equals("happy")){
            textView.setText(data);
        } else{
            drawable.start();
            textView.setText("Bot : Hi");
            textView1.setText("");
        }
        mToast.run();

        int permissions = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if(permissions != PackageManager.PERMISSION_GRANTED){
          //  Log.d("hiii","permissions denied");
            makePermission();
        }
        aiConfiguration = new AIConfiguration("c24be23d932d423ebdb7b98f6b58c460",
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
        aiService = AIService.getService(this, aiConfiguration);
        aiService.setListener(this);
    }
    protected void makePermission(){
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.RECORD_AUDIO},recordeCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case recordeCode:{
                if(grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                   // Log.d("hiii","permissions denied");

                }else{
                 //   Log.d("hiii","permissions Granted");
                }
                return;
            }
        }

    }

    @Override
    public void onResult(AIResponse result) {
        pulseView.finishPulse();

      //  Log.d("hiii", result.toString() );
        query = result.getResult().getResolvedQuery().toString();
        if(query.equals("5") || query.equals("4")|| query.equals("3") ){
           // Log.d("hiii","permissions  yes");
            pulseView.startPulse();
            emoji.setBackgroundResource(R.drawable.saddy);
            drawable = (AnimationDrawable) emoji.getBackground();
            drawable.start();
        } else if(query.equals("2") || (query.equals("1"))){
           // Log.d("hiii","permissions  yes");
            pulseView.startPulse();
            emoji.setBackgroundResource(R.drawable.smiley);
            drawable = (AnimationDrawable) emoji.getBackground();
            drawable.start();
        } else {
          //  Log.d("hiii","permissions  yes");
            pulseView.startPulse();
            emoji.setBackgroundResource(R.drawable.smiley);
            drawable = (AnimationDrawable) emoji.getBackground();
            drawable.start();
        }
      //  Log.d("hiii", query );
        Result result1 = result.getResult();
        res = result1.getAction().toString();


        textView1.setText("Me: "+query);
        textView.setText("Bot: "+result1.getAction().toString());

        tim = true;
        timer = 0;

    }

    public void setIntent(String c){
        drawable.stop();
        pulseView.finishPulse();
        if(c.equals("3")){
            Intent s = new Intent(this, MainActivity.class);
            startActivity(s);
        }
        else if(c.equals("2")){
            Intent s = new Intent(this, YouTubeActivity.class);
            startActivity(s);
        }
        else if((c.equals("4") || c.equals("5") )){
            Intent s = new Intent(this, MusicActivity.class);
            startActivity(s);
        } else if(c.equals("1") || res.equals("Oh,I_Know_what_You_want_From_me......Let's_Play_A_Game") || query.equals("1")){
            Intent s = new Intent(this, HappyGameActivity.class);
            startActivity(s);
        } else if(res.equals("Oh,I_Know_what_To_do_to_end_up_your_Sandness")){
            Intent s = new Intent(this, HappyGameActivity.class);
            startActivity(s);
        }

    }
    @Override
    public void onError(AIError error) {

    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }


    public void ButtonClicked(View view){
        drawable.stop();

        aiService.startListening();
        pulseView.startPulse();



    }
    public final Runnable mToast = new Runnable() {
        @Override
        public void run() {
            mhandler.postDelayed(this, 1000);
            if(tim == true){
                timer = timer + 1;
                if(timer == 3){
                    setIntent(query);
                }
            }

        }
    };

}
