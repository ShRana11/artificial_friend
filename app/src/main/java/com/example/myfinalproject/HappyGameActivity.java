package com.example.myfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

import static android.view.View.INVISIBLE;

public class HappyGameActivity extends AppCompatActivity {
    ImageView myView = null;
    int count = 0;
    final  int[] drawable = new int[]{R.drawable.a1,R.drawable.a2,R.drawable.a3,R.drawable.a4,R.drawable.a5,
            R.drawable.a6,R.drawable.a7,R.drawable.a8,R.drawable.a9 };
    final int draw = R.drawable.a0;
    int[] pos={10,10,10,10,10,10,10,10,10};
    int length = pos.length;
    int getLen = 9;
    int getPos = 10;
    boolean mtimer = false;
    Handler mhandkler = new Handler();
    TextView textView;
    ImageView setView;
    imageAdapter adapter;
    GridView gridView;
    int imageselect = 0;
    int imageNumber = 0;
    ImageView v;
    boolean textset = false;
    boolean startintent = false;
    int gridtime = 3;
    boolean time = false;
    boolean win = false;
    Toast t;
    int currentPos = -1;
    boolean avail = false;
    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_game);
        textView = (TextView)findViewById(R.id.textView);
        setView = (ImageView)findViewById(R.id.setView);
        setView.setVisibility(INVISIBLE);
       final Toast toast= new Toast(this);
         t = toast;
         v = new ImageView(this);



            for (int a = 0; a < length; a++) {
                while (pos[a] == 10) {
                int i = random.nextInt(getLen);
                avail = false;
                for (int j = 0; j < length; j++) {
                    if (pos[j] == i) {
                        // j--;
                        avail = true;
                    }
                }
                if (avail == false && pos.length == 9) {
                    pos[a] = i;
                 //   Log.d("hiii", String.valueOf(pos[a]));
                }
               // getPos = pos.length;
            }
        }

        gridView = (GridView)findViewById(R.id.grid);
        adapter = new imageAdapter(this);
        adapter.getposArray(pos);
        adapter.getResArray(drawable);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(imageselect == 1){
                    adapter.time(0);
                    Getdata(adapter,gridView);
                    currentPos = position;
                    myView = (ImageView)view;
                    int p = drawable[pos[position]];
                    time = true;
                    if(p == imageNumber) {
                       Compare(true);
                       Toast.makeText(getApplicationContext(),"You win", Toast.LENGTH_LONG
                       ).show();
                    }else {
                       Compare(false);
                       Toast.makeText(getApplicationContext(),"You Lose", Toast.LENGTH_LONG
                       ).show();
                    }
                    imageselect = 3;
                }
            }
        });
        mToast.run();
    }
    public final Runnable mToast = new Runnable() {
        @Override
        public void run() {
            if (mtimer == false) {
                mhandkler.postDelayed(this, 1000);
                if(textset == false) {
                    textView.setText(String.valueOf("Time : " + getPos));
                } else {
                    textView.setVisibility(INVISIBLE);
                }
                getPos = getPos - 1;
                if(getPos == 0 && time == false){
                    textset = true;
                    getPos = 10;
                    textView.setEnabled(false);
                    imageselect = 1;
                    adapter.time(10);
                    Getdata(adapter,gridView);
                    setView.setVisibility(View.VISIBLE);
                    int n = random.nextInt(getLen);
                    imageNumber = drawable[pos[n]];
                    setView.setImageResource(drawable[pos[n]]);

                }
                if(time == true && startintent == false){
                   gridtime = gridtime - 1;
                    if (gridtime == 0){

                        if(win == true){
                            gridView.setVisibility(INVISIBLE);
                            setView.setVisibility(INVISIBLE);
                            v.setImageResource(R.drawable.success);
                            t.setView(v);
                            t.show();
                        } else if(win  == false){
                            gridView.setVisibility(INVISIBLE);
                            setView.setVisibility(INVISIBLE);
                            v.setImageResource(R.drawable.lose);
                            t.setView(v);
                            t.show();
                        }
                        gridtime = 3;
                        startintent = true;
                    }

                }
                if(startintent == true){

                    gridtime = gridtime - 1;
                    if(gridtime == 0){
                        mtimer = true;
                        Intent intent = new Intent(HappyGameActivity.this, ChatActivity.class);
                        intent.putExtra("data", "Bot: Did you enjoyed the Game(Yes/No)?");
                        intent.putExtra("check", "happy");
                        startActivity(intent);
                    }
                }
            }
        }
    };

    public void Getdata(Adapter a, GridView gridView){
        gridView.setAdapter((ListAdapter) a);

    }
    public void Compare(boolean  b){
        win = b;

    }
}
