package com.example.myfinalproject;

import android.graphics.Path;

public class Drawing {
    public  int  color;
    public boolean emboss;
    public boolean blur;
    public int strokeWidth;
    public Path path;

    public Drawing(int color, boolean emboss, boolean blur, int strokeWidth, Path path) {
        this.color = color;
        this.emboss = emboss;
        this.blur = blur;
        this.strokeWidth = strokeWidth;
        this.path = path;
    }

}
