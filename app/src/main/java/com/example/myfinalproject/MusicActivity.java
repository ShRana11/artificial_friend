package com.example.myfinalproject;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class MusicActivity extends AppCompatActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    public static final String TAG = "JENELLE";
    ArrayList<String> musiclink = new ArrayList<>();
    Random random = new Random();
    boolean flag = false;
    boolean seek = false;
    int count = 0;
    Button playBtn;
    SeekBar positionBar;
    TextView elapsedTimeLabel;
    TextView remainingTimeLabel;
    MediaPlayer mp = new MediaPlayer();
    int totalTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        musiclink.clear();

        mp.setLooping(true);
        mp.seekTo(0);

        playBtn = (Button) findViewById(R.id.playBtn);
        playBtn.setBackgroundResource(R.drawable.stop);
        positionBar = (SeekBar) findViewById(R.id.positionBar);

        elapsedTimeLabel = (TextView) findViewById(R.id.elapsedTimeLabel);
        remainingTimeLabel = (TextView) findViewById(R.id.remainingTimeLabel);

        positionBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mp.seekTo(progress);
                positionBar.setProgress(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        db.collection("musicLink")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                          //  Log.d(TAG,"finished querying firebase");
                            for (QueryDocumentSnapshot document : task.getResult()) {
                           //     Log.d(TAG, document.getId() + " => " + document.getData());
                                int m = 1;
                                int x = document.getData().size();
                            //    Log.d(TAG, "size: " + x);
                                for(int i = 0; i < x;i++){
                                    String index = String.valueOf(m);
                                    musiclink.add(document.getData().get(index).toString());
                                    m = m+1;
                                }
                                if(musiclink.size() > 0) {
                                    int x1 = random.nextInt(musiclink.size());
                                    try {
                                        mp.setDataSource(musiclink.get(x1));
                                        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                            @Override
                                            public void onPrepared(MediaPlayer mp1) {
                                                mp = mp1;
                                                mp.start();
                                                flag = true;
                                                totalTime = mp.getDuration();
                                                positionBar.setMax(totalTime);
                                            }
                                        });
                                        mp.prepare();
                                    }   catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }


                            }
                        } else {
//                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (seek == false) {
                    while (mp != null) {
                        try {
                            Message msg = new Message();
                            msg.what = mp.getCurrentPosition();
                            handler.sendMessage(msg);
                            count = count + 1;
                            Thread.sleep(998);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
        }).start();

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (seek == false) {
                int currentPosition = msg.what;
                Log.d("shhh", String.valueOf(currentPosition));
                Log.d("shhh", String.valueOf(count));

                positionBar.setProgress(currentPosition);
                String elapsedTime = createTimeLabel(currentPosition);
                elapsedTimeLabel.setText(elapsedTime);

                String remainingTime = createTimeLabel(totalTime - currentPosition);
                remainingTimeLabel.setText("- " + remainingTime);
                if (currentPosition < 1500 && count >= 20 ) {
                    Intent s = new Intent(MusicActivity.this, ChatActivity.class);
                    mp.stop();
                    s.putExtra("data", "Bot: Have You Enjoyed Music(Yes/No)?");
                    s.putExtra("check", "music");
                    startActivity(s);

                    seek = true;
                }
            }
        }
    };


    public String createTimeLabel(int time) {
        String timeLabel = "";
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        timeLabel = min + ":";
        if (sec < 10) timeLabel += "0";
        timeLabel += sec;

        return timeLabel;
    }

    public void playBtnClick(View view) {

        if (!mp.isPlaying()) {
            // Stopping
            mp.start();
            playBtn.setBackgroundResource(R.drawable.stop);

        } else {
            // Playing
            mp.pause();
            playBtn.setBackgroundResource(R.drawable.play);
        }

    }
}
